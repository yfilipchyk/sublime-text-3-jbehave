import sublime, sublime_plugin
import os
import re
from subprocess import call
import threading

settings = None

SETTINGS_NAME = "JBehaveStory.sublime-settings"
PROJECT_PATH = "project_path"
STEPS_PATH = "steps_path"
STORIES_PATH = "stories_path"
ECLIPSE_PATH = "eclipse_path"

steps_cache = None


def update_cache():
	global steps_cache
	steps_cache = StepsCache()
	project_path = settings.get(PROJECT_PATH)
	steps_path = settings.get(STEPS_PATH)
	for steps_folder in steps_path:
		steps_cache.add_steps_from_folder(project_path + steps_folder)
	print("Steps cache is updated")


def plugin_loaded():
	global settings
	settings = sublime.load_settings(SETTINGS_NAME)
	update_cache()


def does_story_step_match_actual(story_step, actual_step):
	step_pattern = get_step_pattern(actual_step)
	return story_step == actual_step or re.match(step_pattern, story_step)


def get_step_pattern(step):
	# params = re.findall(r'\s?(\$\w+|<\w+>)\s?', step)
	params = re.findall(r'\s?(\$\w+)\s?', step)
	step_pattern = str(step)
	for param in params:
		step_pattern = re.sub(re.escape(param), '(.*)', step_pattern, 1)
	# composite steps handling
	if re.match(r'.*: \(\.\*\).*', step_pattern):
		step_pattern = re.sub(r' \(\.\*\)', '(.*)', step_pattern)
	return step_pattern


class OpenStoryCommand(sublime_plugin.TextCommand):
	"""Opens JBehave story found by name in new tab"""

	def run(self, edit):
		self.view.window().show_input_panel("Open", "", self.get_input, None, None)

	def get_input(self, text):
		story = self.find_story(settings.get(PROJECT_PATH) + settings.get(STORIES_PATH), text)
		if story is not None and text in story:
			print("Opening the story ", story)
			self.view.window().open_file(story)

	def find_story(self, stories_dir, story_name):
		files = os.listdir(stories_dir)
		for story in files:
			full_path = os.path.join(stories_dir, story)
			if os.path.isdir(full_path):
				found_file = self.find_story(full_path, story_name)
				if found_file is not None:
					return found_file
			else:
				if story_name in story:
					return full_path
		return None


class OpenJavaFileCommand(sublime_plugin.TextCommand):
	"""Opens Java file"""

	def __init__(self, edit):
		sublime_plugin.TextCommand.__init__(self, edit)
		self.java_view = None
		self.found_step = None

	def run(self, edit):
		view = self.view
		line_region = view.line(view.sel()[0].begin())
		step = view.substr(line_region)
		if step.startswith("And"):
			step = re.sub("And", self.find_parent(view, line_region), step, 1)
		print(step)
		java_file, found_step = steps_cache.find_java_file(get_step_pattern(step))
		self.found_step = found_step
		# print(found_step)
		# open found Java file in new tab and scroll to found step
		if java_file is not None:
			window = self.view.window()
			self.java_view = window.open_file(java_file)
			# wait for Java file loading
			sublime.set_timeout(self.scroll_view, 10)

	def scroll_view(self):
		if self.java_view.is_loading():
			sublime.set_timeout(self.scroll_view, 10)
		else:
			region = self.java_view.find(re.sub(r'\$', r'\\$', '"' + self.found_step + '"'), 0)
			self.java_view.show_at_center(region)
			self.java_view.sel().add(region)

	@staticmethod
	def find_parent(view, step_region):
		start_position = step_region.a
		upper_line_region = view.line(start_position - 1)
		upper_step = view.substr(view.line(start_position - 1))
		match = re.search(r'^(When|Then|Given).*', upper_step)
		if not match and not re.match(r'^Scenario.*', upper_step):
			return OpenJavaFileCommand.find_parent(view, upper_line_region)
		else:
			return match.group(1)


class OpenFileInEclipseCommand(sublime_plugin.TextCommand):
	"""Opens current file in Eclipse"""

	def run(self, edit):
		current_file = self.view.file_name()
		eclipse_path = settings.get(ECLIPSE_PATH)
		call([eclipse_path, "--launcher.openFile", current_file])


class AutoCompleteCollector(sublime_plugin.EventListener):

	def on_post_save_async(self, view):
		update_cache()
		view.run_command("parameters_highlight")
		view.run_command("wrong_steps_highlight", {"scope": "current_region"})
        
	def on_query_completions(self, view, prefix, locations):
		if not view.file_name().endswith(".story"):
			return []
		line = view.substr(view.line(view.sel()[0].begin()))
		if not re.match("({0}|{1}|{2}|{3}).*".format(StepsCache.GIVEN, StepsCache.WHEN, StepsCache.THEN, StepsCache.AND), line):
			return []
		if line.startswith(StepsCache.AND):
			parent_type = OpenJavaFileCommand.find_parent(view, view.line(view.sel()[0].begin()))
			return steps_cache.find_steps(re.sub(StepsCache.AND, parent_type, line, 1), prefix)
		else:
			return steps_cache.find_steps(line, prefix)

	def on_modified_async(self, view):
		view.run_command("parameters_highlight")
		# slows down the editor
		# view.run_command("wrong_steps_highlight", {"scope": "current_region"})

	def on_load_async(self, view):
		view.run_command("parameters_highlight")
		view.run_command("wrong_steps_highlight", {"scope": "whole_buffer"})

	def on_activated_async(self, view):
		view.run_command("parameters_highlight")
	# 	view.run_command("wrong_steps_highlight", {"scope": "whole_buffer"})


class RefreshAllCommand(sublime_plugin.TextCommand):

	def run(self, edit):
		t = threading.Thread(target=self.update_all, args=(self.view,))
		t.setDaemon(True)
		t.start()

	def update_all(self, view):
		update_cache()
		view.run_command("parameters_highlight")
		view.run_command("wrong_steps_highlight", {"scope": "whole_buffer"})


class WrongStepsHighlightCommand(sublime_plugin.TextCommand):

	def run(self, edit, scope):
		view = self.view
		if view.file_name() is None or not view.file_name().endswith(".story"):
			return
		if scope == "whole_buffer":
			regions = self.find_wrong_steps(view, sublime.Region(0, view.size()))
		elif scope == "current_region":
			regions = self.find_wrong_steps(view, view.visible_region())
		else:
			regions = []
		view.add_regions("WrongSteps", regions, "wrongstep")

	def find_wrong_steps(self, view, region):
		regions = []
		for line_region in view.lines(region):
			line = view.substr(line_region)
			if not re.match("({0}|{1}|{2}|{3}).*".format(StepsCache.GIVEN, StepsCache.WHEN, StepsCache.THEN, StepsCache.AND), line):
				continue
			if line.startswith(StepsCache.AND):
				parent_type = OpenJavaFileCommand.find_parent(view, line_region)
				line = re.sub(StepsCache.AND, parent_type, line, 1)
			if steps_cache.find_java_file(line) == (None, None):
				regions.append(line_region)
		return regions


class ParametersHighlightCommand(sublime_plugin.TextCommand):

	def run(self, edit):
		view = self.view
		if view.file_name() is None or not view.file_name().endswith(".story"):
			return
		view.add_regions("StepParameters", self.find_params(view), "actual", flags=sublime.DRAW_NO_OUTLINE)

	def find_params(self, view):
		regions = []
		for step_pattern in steps_cache.get_all_patterns():
			if "(.*)" in step_pattern:
				found_steps = view.find_all(step_pattern)
				if len(found_steps) > 0:
					for step in found_steps:
						match = re.finditer(step_pattern, view.substr(step))
						for m in match:
							parameter = m.group(1)
							if parameter != "" and parameter != " " and not parameter.startswith("$"):
								step.a = step.a + m.start(1)
								step.b = step.a + len(parameter)
								regions.append(step)
		return regions


class StepsCache(object):
    """
Holds the steps cache. Provides functionality for extracting steps from Java source, from Java file and from
directory."""

    GIVEN = "Given"
    WHEN = "When"
    THEN = "Then"
    ALIAS = "Alias"
    ALIASES = "Aliases"
    AND = "And"

    def __init__(self):
        self.given_steps = {}
        self.when_steps = {}
        self.then_steps = {}
        self.files = {}

    def add_step(self, step_type, step_pattern, step, java_class):
        """Adds step to cache"""

        if step_type == StepsCache.GIVEN:
            self.given_steps[step_pattern] = [step, java_class]
        elif step_type == StepsCache.WHEN:
            self.when_steps[step_pattern] = [step, java_class]
        elif step_type == StepsCache.THEN:
            self.then_steps[step_pattern] = [step, java_class]
        else:
            pass

    def add_steps_from_source(self, java_source, file_name):
        """Finds steps in java source and stores them in the cache"""

        # remember file name and package
        package = re.findall(r'package (.*);', java_source)[0]
        java_class = package + "." + re.findall(r'.*/|\\(\w+)\.java', file_name)[0]
        self.files[java_class] = file_name

        # finding all steps
        steps = re.findall(r'\s*(@(When|Then|Given|Alias|Aliases)\((.*)\))\s*', java_source)
        for i in range(len(steps)):
            step = steps[i][0]
            # handling the Aliases annotation
            if step.startswith("@" + StepsCache.ALIASES) and i != 0:
                aliases = re.findall(r'"([^"]+)"', step)
                # finding step type which alias refers to
                match = re.search(r'^@(When|Then|Given)\("(.*)"\)$', steps[i-1][0])
                if match:
                    step_type = match.group(1)
                    for alias in aliases:
                        self.add_step(step_type, get_step_pattern(alias), alias, java_class)
            # handling the Alias annotation
            elif step.startswith("@" + StepsCache.ALIAS) and i != 0:
                # finding step type which alias refers to
                match = re.search(r'^@(When|Then|Given)\("(.*)"\)$', steps[i-1][0])
                if match:
                    step_type = match.group(1)
                    match = re.search(r'^@(Alias)\("(.*)"\)$', step)
                    if match:
                        self.add_step(step_type, get_step_pattern(match.group(2)), match.group(2), java_class)
            else:
                match = re.search(r'^@(When|Then|Given)\("(.*)"\)$', step)
                if match:
                    self.add_step(match.group(1), get_step_pattern(match.group(2)), match.group(2), java_class)

    def add_steps_from_file(self, file_path):
        """Extracts steps from provided file"""

        with open(file_path) as source:
            self.add_steps_from_source(source.read(), file_path)

    def add_steps_from_folder(self, folder_path):
        """Extracts steps from files from provided directory"""

        for file in os.listdir(folder_path):
            if os.path.isdir(os.path.join(folder_path, file)):
                self.add_steps_from_folder(os.path.join(folder_path, file))
            else:
                if file.endswith(".java"):
                    self.add_steps_from_file(os.path.join(folder_path, file))

    def find_java_file(self, step):
        """Returns Java file where provided step is implemented"""

        match = re.search(r'(When|Then|Given)\s(.*)', step)
        if match:
            if match.group(1) == StepsCache.GIVEN:
                return self.get_java_file(self.given_steps, match.group(2))
            if match.group(1) == StepsCache.WHEN:
                return self.get_java_file(self.when_steps, match.group(2))
            if match.group(1) == StepsCache.THEN:
                return self.get_java_file(self.then_steps, match.group(2))
        return None

    def get_java_file(self, steps_dict, step):
        try:
            found_step = self.when_steps[step]
            return (self.files[found_step[1]], found_step[0])
        except KeyError:
            for step_pattern in steps_dict.keys():
            	match = re.match(step_pattern, step)
            	if match:
            		if len(match.groups()) == 0 and len(step) != len(step_pattern):
            			continue
            		found_step = steps_dict[step_pattern]
            		return (self.files[found_step[1]], found_step[0])
        return (None, None)

    def get_autosuggestions_list(self, steps_dict, step, prefix_len):
    	autosuggestions = []
    	for step_pattern in steps_dict.keys():
    		if step.lower() == step_pattern.lower() or re.match(step_pattern, step, re.I) \
    			or steps_dict[step_pattern][0].lower().startswith(step.lower()):
    			to_show = steps_dict[step_pattern][0]
    			to_insert = steps_dict[step_pattern][0][prefix_len:]
    			autosuggestions.append((to_show, re.sub(r'\$', r'\$', to_insert)))
    	return autosuggestions

    def has_similar_words(self, step, step_candidate):
    	similar_words_count = 0
    	words = step.split()
    	for word in words:
    		if word in step_candidate:
    			similar_words_count += 1
    	if len(step) <= len(step_candidate) and similar_words_count >= len(step_candidate[0:len(step)].split()):
    		return True
    	else:
    		return False

    def find_steps(self, step, prefix):
    	match = re.search(r'(When|Then|Given)\s(.*)', step)
    	if match:
    		prefix_len = len(step) - len(match.group(1)) - len(prefix) - 1
    		if match.group(1) == StepsCache.GIVEN:
    			return self.get_autosuggestions_list(self.given_steps, match.group(2), prefix_len)
    		elif match.group(1) == StepsCache.WHEN:
    			return self.get_autosuggestions_list(self.when_steps, match.group(2), prefix_len)
    		elif match.group(1) == StepsCache.THEN:
    			return self.get_autosuggestions_list(self.then_steps, match.group(2), prefix_len)
    		else:
    			return []
    	return []

    def get_all_patterns(self):
    	return list(self.given_steps) + list(self.when_steps) + list(self.then_steps)