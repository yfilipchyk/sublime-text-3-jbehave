import sublime, sublime_plugin
import os
import re
import webbrowser
from subprocess import Popen, PIPE
import threading


settings = None

SETTINGS_NAME = "JBehaveStory.sublime-settings"
PROJECT_PATH = "project_path"
STEPS_PATH = "steps_path"
STORIES_PATH = "stories_path"
MAVEN_BIN = "maven_path"
JIRA_BROWSE = "jira_browse"

STORY_FOLDER = "story.folder"
STORY_FILTER = "story.filter"
TEST_URI = "test.uri"
WORKSPACE_NAME = "test.workspaceName"
LOCAL = "local"
XQADEV = "xqadev"
XSTAGING = "xstaging"
XTEST = "xtest"
XDEVTINY = "xdevtiny"
PORTS = {
	8: 1443,
	10: 4443,
}

DEFAULT_URLS = {
	LOCAL: "https://www.thxcloud.com",
	XQADEV: "https://xqadev.thunderhead.com",
	XSTAGING: "https://xstaging.thunderhead.com",
	XDEVTINY: "https://xdevtiny{0}.thunderhead.com",
	XTEST: "https://xtest.thunderhead.com",
}

CONFIG_KEYS = [
	STORY_FOLDER,
	STORY_FILTER,
	TEST_URI,
	WORKSPACE_NAME
]

DEFAULT_CONFIG = {
	STORY_FOLDER: "",
	STORY_FILTER: "",
	TEST_URI: XQADEV,
	WORKSPACE_NAME: "",
}

# holds Popen with maven
AUTOMATION_RUN = None


def plugin_loaded():
	global settings
	settings = sublime.load_settings(SETTINGS_NAME)


class StopAutomationCommand(sublime_plugin.TextCommand):
	"""Stops the automation"""

	def run(self, edit):
		global AUTOMATION_RUN
		if AUTOMATION_RUN is not None:
			AUTOMATION_RUN.kill()


class RunStoryCommand(sublime_plugin.TextCommand):
	"""Runs the automation"""

	def __init__(self, edit):
		sublime_plugin.TextCommand.__init__(self, edit)
		self.current_file = None

	def run(self, edit, environment, instance):
		self.current_file = self.view.file_name()
		print(self.current_file)
		story = DEFAULT_CONFIG[STORY_FILTER]
		folder = DEFAULT_CONFIG[STORY_FOLDER]
		# getting story filter and folder
		match = re.search(r'(\\|/)(\w+)\.story', self.current_file)
		if match:
			story = match.group(2)
			match = re.search(r'stories(\\|/)(.*)(\\|/)' + story + r'\.story', self.current_file)
			if match:
				folder = re.sub(r'\\', r'/', match.group(2))
		# overriding the default config
		config = dict(DEFAULT_CONFIG)
		config[STORY_FOLDER] = folder
		config[STORY_FILTER] = story
		if environment == XDEVTINY:
			if instance == 10 or instance == 8:
				config[TEST_URI] = DEFAULT_URLS[XDEVTINY].format(instance) + ":" + str(PORTS[instance])
			else:
				config[TEST_URI] = DEFAULT_URLS[XDEVTINY].format(instance)
		else:
			config[TEST_URI] = DEFAULT_URLS[environment]
		"creating command line args"
		args = ""
		for key in CONFIG_KEYS:
			if config[key] != "" or key == WORKSPACE_NAME:
				args += "-D" + key + "=" + config[key] + " "
		if config[STORY_FILTER] != "" and config[STORY_FOLDER] != "":
			# opening input panel with default settings
			self.view.window().show_input_panel("Run", args[0:-1], self.get_input, None, None)

	def get_input(self, text):
		"""Handles the input panel submitting"""
		args = r'{0}{1}mvn clean install -Dtest.contextView=false {2}'.format(settings.get(MAVEN_BIN), os.sep, text)
		print(args)
		global AUTOMATION_RUN
		AUTOMATION_RUN = Popen(args, stdout=PIPE, universal_newlines=True, cwd=settings.get(PROJECT_PATH), shell=True)
		# new thread will read the output of maven process
		t = threading.Thread(target=self.read_output)
		t.setDaemon(True)
		t.start()

	def read_output(self):
		"""Prints the process output to Sublime console"""
		global AUTOMATION_RUN
		while AUTOMATION_RUN is not None and not AUTOMATION_RUN.poll():
			line = re.sub(r'(\n|\r)$', r'', AUTOMATION_RUN.stdout.readline())
			if line != "":
				print(line)		
		print("Automation was stopped")


class OpenJiraIssue(sublime_plugin.TextCommand):
	"""Opens Jira issue in default browser"""

	def run(self, edit):
		view = self.view
		line = view.substr(view.word(view.sel()[0].begin()))
		match = re.search(r'((thx|ofmr)(\d+))', line, re.IGNORECASE)
		if match and len(match.groups()) > 2:
			issue = match.group(2).upper() + "-" + match.group(3)
			view.set_status("issue", issue)
			webbrowser.open_new_tab(settings.get(JIRA_BROWSE) + issue)
		else:
			# if cursor is not on line with Jira issue plugin will try to find step in Java
			view.run_command("open_java_file")